#!/usr/bin/env bash

function getMount  () {
	if [[ -z $1 ]]; then
		echo "pass in a project to get its mounts"
		exit 1
	else
		local project=${1}
	fi
	if [[ -z $2 ]]; then
		echo "pass in a second var with volume name"
		exit 1
	else
		local volume=${2}
	fi

	oc get dc/${project} -o jsonpath="{ .spec.template.spec.containers[?(@.name==\"${project}\")].volumeMounts[?(@.name==\"${volume}\")].mountPath }"
}

function getPods () {
	if [[ -z $1 ]]; then
		echo "pass in a project to get its pods"
		exit 1
	else
		local project=${1}
	fi
	oc get pod --selector=deploymentconfig=${project} -o jsonpath='{ range .items[*]}{.metadata.name }'
}

function getVolumes () {
	if [[ -z ${1} ]]; then
		echo "pass in a project to get its volumes"
	else
		local project=${1}
	fi
}

set -eux 

project=${1}

echo $(getVolumes ${project})

for pod in $(getPods ${project}); do
	mkdir ${pod};
	oc rsync ${pod}:$(getMount ${project} nbc9-python-8ball-volume) ${pod}/.;
done
